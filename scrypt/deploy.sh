#!/usr/bin/env bash

mvn clean package

echo 'Copy'

scp -i ~/.ssh/id_rsa \
target/Department.jar \
root@185.22.60.136:/home

echo 'restart'

ssh -i ~/.ssh/id_rsa root@185.22.60.136 << EOF

pgrep java | xargs kill -9
nohup java -jar Department.jar > log.txt &

EOF

echo 'Bye'