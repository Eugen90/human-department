package org.bip.department.controller;

import org.bip.department.domain.Employee;
import org.bip.department.domain.Message;
import org.bip.department.service.EmployeeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class EmployeeController {
    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/staffList")
    public String getStaffList(@RequestParam(name = "message", required = false, defaultValue = "") String message,
                               Model model){
        if (message.equals("delete")) model.addAttribute("message", Message.GetDeleteMessage(true));
        model.addAttribute("employees", employeeService.findAll());
        return "staffList";
    }

    @GetMapping("/employeeEdit")
    public String employeeEdit(@RequestParam(name = "id",required = false, defaultValue = "0") long employeeId,
                               @RequestParam(name = "message", required = false, defaultValue = "") String message,
                               Model model){
        if (message.equals("delete")) model.addAttribute("message", Message.GetSaveMessage(true));
        model.addAttribute("employee", employeeService.findEmployee(employeeId));
        return "employeeEdit";
    }

    @PostMapping("/employeeEdit")
    public String saveEmployee(
            @Valid Employee employee,
            BindingResult bindingResult,
            Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
        } else {
            employee = employeeService.saveEmployee(employee);
            model.addAttribute("message", Message.GetSaveMessage(true));
        }

        model.addAttribute("employee", employee);
        return "employeeEdit";
    }

    @DeleteMapping("/staffDelete")
    public String deleteEmployee(@RequestParam(name = "id") String id){
        employeeService.deleteEmployee(id);

        return "redirect:/staffList?employeeId=" + id + "&message=delete";
    }
}
