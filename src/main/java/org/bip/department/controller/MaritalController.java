package org.bip.department.controller;

import org.bip.department.domain.Kin;
import org.bip.department.domain.Marital;
import org.bip.department.domain.Message;
import org.bip.department.service.MaritalService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class MaritalController {
    private final MaritalService maritalService;

    public MaritalController(MaritalService maritalService) {
        this.maritalService = maritalService;
    }

    @GetMapping("/marital")
    public String getMarital(@RequestParam(name = "employeeId") long employeeId,
                             @RequestParam(name = "message", required = false, defaultValue = "") String message,
                             Model model){
        if (message.equals("save")) model.addAttribute("message", Message.GetSaveMessage(true));
        if (message.equals("delete")) model.addAttribute("message", Message.GetDeleteMessage(true));
        model.addAttribute("marital", maritalService.getEmployeeMarital(employeeId));
        model.addAttribute("employeeId", employeeId);
        return "marital";
    }

    @GetMapping("/kinEdit")
    public String kinEdit(@RequestParam(name = "id",required = false, defaultValue = "0") long kinId,
                          @RequestParam(name = "maritalId") long maritalId,
                          @RequestParam(name = "employeeId") long employeeId,
                          Model model){

        model.addAttribute("maritalId", maritalId);
        model.addAttribute("employeeId", employeeId);
        model.addAttribute("kin", maritalService.getKin(kinId));
        return "kinEdit";
    }

    @PostMapping("/marital")
    public String saveMarital(@RequestParam(name = "employeeId") long id,
                              @Valid Marital marital,
                              BindingResult bindingResult,
                              Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("marital", marital);
            model.addAttribute("employeeId", id);
            return "marital";
        }
        maritalService.saveMarital(marital, id);

        return "redirect:/employeeEdit?employeeId=" + id + "&message=save";
    }

    @PostMapping("/kinEdit")
    public String saveKin(@RequestParam(name = "maritalId") long maritalId,
                          @RequestParam(name = "employeeId") long employeeId,
                          @Valid Kin kin,
                          BindingResult bindingResult,
                          Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.addAttribute("employeeId", employeeId);
            model.addAttribute("kin", kin);
            model.addAttribute("maritalId", maritalId);
            model.mergeAttributes(errorsMap);
            return "kinEdit";
        }
        maritalService.saveKin(kin, maritalId);

        return "redirect:/marital?employeeId=" + employeeId + "&message=save";
    }

    @DeleteMapping("/kinEdit")
    public String deleteKin(@RequestParam(name = "id") long id,
                            @RequestParam(name = "employeeId") long employeeId){
        maritalService.deleteKin(id);

        return "redirect:/marital?employeeId=" + employeeId + "&message=delete";
    }
}
