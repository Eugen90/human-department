package org.bip.department.controller;

import org.bip.department.domain.Message;
import org.bip.department.domain.User;
import org.bip.department.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/userList")
    public String getUserList(@RequestParam(name = "message", required = false, defaultValue = "") String message ,
                              Model model){
        if (message.equals("delete")) model.addAttribute("message", Message.GetDeleteMessage(true));
        model.addAttribute("users", userService.findAll());
        return "userList";
    }

    @GetMapping("/userEdit")
    public String getUser(@RequestParam(name = "name", required = false) String username, Model model){
        User user = username != null?(User) userService.loadUserByUsername(username): null;
        model.addAttribute("user", user);
        return "userEdit";
    }

    @PostMapping("/userEdit")
    public String saveUser(@Valid User user,
                           BindingResult bindingResult,
                           Model model){
       if (bindingResult.hasErrors()){
           Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

           model.mergeAttributes(errorsMap);
           model.addAttribute("user", user);
           return "userEdit";
       }
       userService.saveUser(user);

        return "redirect:/login";
    }

    @GetMapping("/activateUser")
    public String activateUser(@RequestParam(name = "username") String username){
        userService.activateUser(username);

        return "redirect:/userList?message=activated";
    }

    @DeleteMapping("/userEdit")
    public String deleteUser(@RequestParam(name = "id") long userId){
        userService.deleteUser(userId);

        return "redirect:/userList?message=delete";
    }
}
