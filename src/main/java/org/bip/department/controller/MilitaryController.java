package org.bip.department.controller;

import org.bip.department.domain.Military;
import org.bip.department.service.EmployeeService;
import org.bip.department.service.MilitaryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class MilitaryController {
    private final MilitaryService militaryService;
    private final EmployeeService employeeService;

    public MilitaryController(MilitaryService militaryService, EmployeeService employeeService) {
        this.militaryService = militaryService;
        this.employeeService = employeeService;
    }

    @GetMapping("/military")
        public String getMilitary(@RequestParam(name = "employeeId") long employeeId,
                                  Model model){
            model.addAttribute("military", militaryService.getEmployeeMilitary(employeeId));
            model.addAttribute("employeeId", employeeId);
        return "military";
        }

    @PostMapping("/military")
    public String savePassport(@RequestParam(name = "employeeId") long id,
                               @Valid Military military,
                               BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("military", military);
            model.addAttribute("employeeId", id);
            return "military";
        }
        militaryService.saveMilitary(military, id);

        return "redirect:/employeeEdit?employeeId=" + id + "&message=save";
    }
}
