package org.bip.department.controller;

import org.bip.department.domain.Message;
import org.bip.department.domain.Telephone;
import org.bip.department.service.TelephoneService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class TelephoneController {
    private final TelephoneService telephoneService;

    public TelephoneController(TelephoneService telephoneService) {
        this.telephoneService = telephoneService;
    }

    @GetMapping("/telephone")
    public String getTelephones(@RequestParam(name = "employeeId") long employeeId,
                                @RequestParam(name = "message", required = false, defaultValue = "") String message,
                                Model model){
        if (message.equals("save")) model.addAttribute("message", Message.GetSaveMessage(true));
        if (message.equals("delete")) model.addAttribute("message", Message.GetDeleteMessage(true));
        model.addAttribute("telephones", telephoneService.findAll(employeeId));
        model.addAttribute("employeeId", employeeId);
        return "telephone";
    }

    @GetMapping("/telephoneEdit")
    public String getTelephone(@RequestParam(name = "telephoneId", required = false, defaultValue = "0") long telephoneId,
                               @RequestParam(name = "employeeId") long employeeId,
                               Model model){
        model.addAttribute("telephone", telephoneService.findTelephone(telephoneId));
        model.addAttribute("employeeId", employeeId);
        return "telephoneEdit";
    }

    @PostMapping("/telephoneEdit")
    public String saveTelephone(@RequestParam(name = "employeeId") long employeeId,
                                @Valid Telephone telephone,
                                BindingResult bindingResult,
                                Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("telephone", telephone);
            return "telephoneEdit";
        }
        telephoneService.saveTelephone(telephone, employeeId);

        return "redirect:/telephone?employeeId=" + employeeId + "&message=save";
    }

    @DeleteMapping("/telephoneEdit")
    public String deleteTelephone(@RequestParam(name = "id") long telephoneId,
                                  @RequestParam(name = "employeeId") long employeeId){
        telephoneService.deleteTelephone(telephoneId);

        return "redirect:/telephone?employeeId=" + employeeId + "&message=delete";
    }
}
