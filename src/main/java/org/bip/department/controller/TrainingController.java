package org.bip.department.controller;

import org.bip.department.domain.Message;
import org.bip.department.domain.Training;
import org.bip.department.service.TrainingService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class TrainingController {
    private final TrainingService trainingService;

    public TrainingController(TrainingService trainingService) {
        this.trainingService = trainingService;
    }

    @GetMapping("/training")
    public String getTrainings(@RequestParam(name = "employeeId") long employeeId,
                               @RequestParam(name = "message", required = false, defaultValue = "") String message,
                               Model model){
        if (message.equals("save")) model.addAttribute("message", Message.GetSaveMessage(true));
        if (message.equals("delete")) model.addAttribute("message", Message.GetDeleteMessage(true));
        model.addAttribute("trainings", trainingService.findAll(employeeId));
        model.addAttribute("employeeId", employeeId);
        return "training";
    }

    @GetMapping("/trainingEdit")
    public String getTraining(@RequestParam(name = "trainingId", required = false, defaultValue = "0") long trainingId,
                               @RequestParam(name = "employeeId") long employeeId,
                               Model model){
        model.addAttribute("training", trainingService.findTraining(trainingId));
        model.addAttribute("employeeId", employeeId);
        return "trainingEdit";
    }

    @PostMapping("/trainingEdit")
    public String saveTraining(@RequestParam(name = "employeeId") long employeeId,
                               @Valid Training training,
                               BindingResult bindingResult,
                               Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("training", training);
            return "trainingEdit";
        }
        trainingService.saveTraining(training, employeeId);

        return "redirect:/training?employeeId=" + employeeId + "&message=save";
    }

    @DeleteMapping("/trainingEdit")
    public String deleteTraining(@RequestParam(name = "id") long trainingId,
                                  @RequestParam(name = "employeeId") long employeeId){
        trainingService.deleteTraining(trainingId);

        return "redirect:/training?employeeId=" + employeeId + "&message=delete";
    }
}
