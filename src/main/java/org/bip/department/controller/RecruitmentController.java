package org.bip.department.controller;

import org.bip.department.domain.Message;
import org.bip.department.domain.Recruitment;
import org.bip.department.service.RecruitmentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class RecruitmentController {
    private final RecruitmentService recruitmentService;

    public RecruitmentController(RecruitmentService recruitmentService) {
        this.recruitmentService = recruitmentService;
    }

    @GetMapping("/recruitment")
    public String getRecruitments(@RequestParam(name = "employeeId") long employeeId,
                                  @RequestParam(name = "message", required = false, defaultValue = "") String message,
                                  Model model){
        if (message.equals("save")) model.addAttribute("message", Message.GetSaveMessage(true));
        if (message.equals("delete")) model.addAttribute("message", Message.GetDeleteMessage(true));
        model.addAttribute("recruitments", recruitmentService.findAll(employeeId));
        model.addAttribute("employeeId", employeeId);
        return "recruitment";
    }

    @GetMapping("/recruitmentEdit")
    public String getRecruitment(@RequestParam(name = "recruitmentId", required = false, defaultValue = "0") long recruitmentId,
                               @RequestParam(name = "employeeId") long employeeId,
                               Model model){
        model.addAttribute("recruitment", recruitmentService.findRecruitment(recruitmentId));
        model.addAttribute("employeeId", employeeId);
        return "recruitmentEdit";
    }

    @PostMapping("/recruitmentEdit")
    public String saveRecruitment(@RequestParam(name = "employeeId") long employeeId,
                                  @Valid Recruitment recruitment,
                                  BindingResult bindingResult,
                                  Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("recruitment", recruitment);
            return "recruitmentEdit";
        }
        recruitmentService.saveRecruitment(recruitment, employeeId);

        return "redirect:/recruitment?employeeId=" + employeeId + "&message=save";
    }

    @DeleteMapping("/recruitmentEdit")
    public String deleteTelephone(@RequestParam(name = "id") long recruitmentId,
                                  @RequestParam(name = "employeeId") long employeeId){
        recruitmentService.deleteRecruitment(recruitmentId);

        return "redirect:/recruitment?employeeId=" + employeeId + "&message=delete";
    }
}
