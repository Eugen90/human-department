package org.bip.department.controller;

import org.bip.department.domain.Education;
import org.bip.department.domain.Message;
import org.bip.department.service.EducationService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class EducationController {
    public final EducationService educationService;

    public EducationController(EducationService educationService) {
        this.educationService = educationService;
    }

    @GetMapping("/education")
    public String getEducations(@RequestParam(name = "employeeId") long employeeId,
                                @RequestParam(name = "message", required = false, defaultValue = "") String message,
                                Model model){
        if (message.equals("save")) model.addAttribute("message", Message.GetSaveMessage(true));
        if (message.equals("delete")) model.addAttribute("message", Message.GetDeleteMessage(true));
        model.addAttribute("educations", educationService.findAll(employeeId));
        model.addAttribute("employeeId", employeeId);
        return "education";
    }

    @GetMapping("/educationEdit")
    public String getEducation(@RequestParam(name = "educationId", required = false, defaultValue = "0") long educationId,
                               @RequestParam(name = "employeeId") long employeeId,
                               Model model){
        model.addAttribute("education", educationService.findEducation(educationId));
        model.addAttribute("employeeId", employeeId);
        return "educationEdit";
    }

    @PostMapping("/educationEdit")
    public String saveEducation(@RequestParam(name = "employeeId") long employeeId,
                                @Valid Education education,
                                BindingResult bindingResult,
                                Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("education", education);
            return "educationEdit";
        }
            educationService.saveEducation(education, employeeId);

        return "redirect:/education?employeeId=" + employeeId + "&message=save";
    }

    @DeleteMapping("/educationEdit")
    public String deleteEducation(@RequestParam(name = "id") long educationId,
                                  @RequestParam(name = "employeeId") long employeeId){
        educationService.deleteEducation(educationId);

        return "redirect:/education?employeeId=" + employeeId + "&message=delete";
    }
}
