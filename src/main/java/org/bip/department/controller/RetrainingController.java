package org.bip.department.controller;

import org.bip.department.domain.Message;
import org.bip.department.domain.Retraining;
import org.bip.department.service.RetrainingService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class RetrainingController {
    private final RetrainingService retrainingService;

    public RetrainingController(RetrainingService retrainingService) {
        this.retrainingService = retrainingService;
    }

    @GetMapping("/retraining")
    public String getRetrainings(@RequestParam(name = "employeeId") long employeeId,
                                 @RequestParam(name = "message", required = false, defaultValue = "") String message,
                                 Model model){
        if (message.equals("save")) model.addAttribute("message", Message.GetSaveMessage(true));
        if (message.equals("delete")) model.addAttribute("message", Message.GetDeleteMessage(true));
        model.addAttribute("retrainings", retrainingService.findAll(employeeId));
        model.addAttribute("employeeId", employeeId);
        return "retraining";
    }

    @GetMapping("/retrainingEdit")
    public String getRetraining(@RequestParam(name = "retrainingId", required = false, defaultValue = "0") long retrainingId,
                               @RequestParam(name = "employeeId") long employeeId,
                               Model model){
        model.addAttribute("retraining", retrainingService.findRetraining(retrainingId));
        model.addAttribute("employeeId", employeeId);
        return "retrainingEdit";
    }

    @PostMapping("/retrainingEdit")
    public String saveRetraining(@RequestParam(name = "employeeId") long employeeId,
                                 @Valid Retraining retraining,
                                 BindingResult bindingResult,
                                 Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("retraining", retraining);
            return "retrainingEdit";
        }
        retrainingService.saveRetraining(retraining, employeeId);

        return "redirect:/retraining?employeeId=" + employeeId + "&message=save";
    }

    @DeleteMapping("/retrainingEdit")
    public String deleteRetraining(@RequestParam(name = "id") long retrainingId,
                                  @RequestParam(name = "employeeId") long employeeId){
        retrainingService.deleteRetraining(retrainingId);

        return "redirect:/retrainig?employeeId=" + employeeId + "&message=delete";
    }
}
