package org.bip.department.controller;

import org.bip.department.domain.ForeignLanguage;
import org.bip.department.domain.Message;
import org.bip.department.service.LanguageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class LanguageController {
    private final LanguageService languageService;

    public LanguageController(LanguageService languageService) {
        this.languageService = languageService;
    }

    @GetMapping("/language")
    public String getLanguages(@RequestParam(name = "employeeId") long employeeId,
                               @RequestParam(name = "message", required = false, defaultValue = "") String message,
                               Model model){
        if (message.equals("save")) model.addAttribute("message", Message.GetSaveMessage(true));
        if (message.equals("delete")) model.addAttribute("message", Message.GetDeleteMessage(true));
        model.addAttribute("languages", languageService.findAll(employeeId));
        model.addAttribute("employeeId", employeeId);
        return "language";
    }

    @GetMapping("/languageEdit")
    public String getLanguage(@RequestParam(name = "languageId", required = false, defaultValue = "0") long languageId,
                              @RequestParam(name = "employeeId") long employeeId,
                              Model model){
        model.addAttribute("foreignLanguage", languageService.findLanguage(languageId));
        model.addAttribute("employeeId", employeeId);
        return "languageEdit";
    }

    @PostMapping("/languageEdit")
    public String saveLanguage(@RequestParam(name = "employeeId") long employeeId,
                               @Valid ForeignLanguage foreignLanguage,
                               BindingResult bindingResult,
                               Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("foreignLanguage", foreignLanguage);
            return "languageEdit";
        }
        languageService.saveLanguage(foreignLanguage, employeeId);

        return "redirect:/language?employeeId=" + employeeId + "&message=save";
    }

    @DeleteMapping("/languageEdit")
    public String deleteLanguage(@RequestParam(name = "id") long languageId,
                                  @RequestParam(name = "employeeId") long employeeId){
        languageService.deleteLanguage(languageId);

        return "redirect:/language?employeeId=" + employeeId + "&message=delete";
    }
}
