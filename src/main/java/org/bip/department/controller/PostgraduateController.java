package org.bip.department.controller;

import org.bip.department.domain.Postgraduate;
import org.bip.department.service.PostgraduateService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class PostgraduateController {
    private final PostgraduateService postgraduateService;

    public PostgraduateController(PostgraduateService postgraduateService) {
        this.postgraduateService = postgraduateService;
    }

    @GetMapping("/postgraduate")
    public String getPostgraduate(@RequestParam(name = "employeeId") long employeeId,
                                  Model model){
        model.addAttribute("postgraduate", postgraduateService.findPostgraduate(employeeId));
        model.addAttribute("employeeId", employeeId);
        return "postgraduate";
    }

    @PostMapping("/postgraduate")
    public String savePostgraduate(@RequestParam(name = "employeeId") long id,
                                   @Valid Postgraduate postgraduate,
                                   BindingResult bindingResult,
                                   Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("postgraduate", postgraduate);
            model.addAttribute("employeeId", id);
            return "postgraduate";
        }
        postgraduateService.savePostgraduate(postgraduate, id);

        return "redirect:/employeeEdit?employeeId=" + id + "&message=save";
    }
}
