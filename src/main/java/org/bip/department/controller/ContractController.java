package org.bip.department.controller;

import org.bip.department.domain.Contract;
import org.bip.department.service.ContractService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class ContractController {
    private final ContractService contractService;

    public ContractController(ContractService contractService) {
        this.contractService = contractService;
    }

    @GetMapping("/contract")
    public String getContract(@RequestParam(name = "employeeId") long employeeId,
                              Model model){
        model.addAttribute("contract", contractService.findContract(employeeId));
        model.addAttribute("employeeId", employeeId);
        return "contract";
    }

    @PostMapping("/contract")
    public String saveContract(@RequestParam(name = "employeeId") long id,
                               @Valid Contract contract,
                               BindingResult bindingResult,
                               Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("contract", contract);
            model.addAttribute("employeeId", id);
            return "contract";
        }
            contractService.saveContract(contract, id);

        return "redirect:/employeeEdit?employeeId=" + id + "&message=save";
    }
}
