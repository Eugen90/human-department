package org.bip.department.controller;

import org.bip.department.domain.Passport;
import org.bip.department.service.PassportService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class PassportController {
    private final PassportService passportService;

    public PassportController(PassportService passportService) {
        this.passportService = passportService;
    }

    @GetMapping("/passport")
    public String getPassport(@RequestParam(name = "employeeId") long employeeId, Model model){
        model.addAttribute("passport", passportService.getEmployeePassport(employeeId));
        model.addAttribute("employeeId", employeeId);
        return "passport";
    }

    @PostMapping("/passport")
    public String savePassport(@RequestParam(name = "employeeId") long id,
                               @Valid Passport passport,
                               BindingResult bindingResult,
                               Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("passport", passport);
            model.addAttribute("employeeId", id);
            return "passport";
        }
        passportService.savePassport(passport, id);

        return "redirect:/employeeEdit?employeeId=" + id + "&message=save";
    }
}
