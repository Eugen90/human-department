package org.bip.department.controller;

import org.bip.department.domain.Attestation;
import org.bip.department.domain.Message;
import org.bip.department.service.AttestationService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class AttestationController {
    private final AttestationService attestationService;

    public AttestationController(AttestationService attestationService) {
        this.attestationService = attestationService;
    }

    @GetMapping("/attestation")
    public String getAttestations(@RequestParam(name = "employeeId") long employeeId,
                                  @RequestParam(name = "message", required = false, defaultValue = "") String message,
                                  Model model){
        if (message.equals("save")) model.addAttribute("message", Message.GetSaveMessage(true));
        if (message.equals("delete")) model.addAttribute("message", Message.GetDeleteMessage(true));
        model.addAttribute("attestations", attestationService.findAll(employeeId));
        model.addAttribute("employeeId", employeeId);
        return "attestation";
    }

    @GetMapping("/attestationEdit")
    public String getAttestation(@RequestParam(name = "attestationId", required = false, defaultValue = "0") long attestationId,
                                 @RequestParam(name = "employeeId") String employeeId,
                                 Model model){
        model.addAttribute("attestation", attestationService.findAttestation(attestationId));
        model.addAttribute("employeeId", employeeId);
        return "attestationEdit";
    }

    @PostMapping("/attestationEdit")
    public String saveAttestation(@RequestParam(name = "employeeId") long employeeId,
                                  @Valid Attestation attestation,
                                  BindingResult bindingResult,
                                  Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("attestation", attestation);
            model.addAttribute("employeeId", employeeId);
            return "attestationEdit";
        }
        attestationService.saveAttestation(attestation, employeeId);

        return "redirect:/attestation?employeeId=" + employeeId + "&message=save";
    }

    @DeleteMapping("/attestationEdit")
    public String deleteAttestation(@RequestParam(name = "id") long attestationId,
                                    @RequestParam(name = "employeeId") long employeeId){
        attestationService.deleteAttestation(attestationId);

        return "redirect:/attestation?employeeId=" + employeeId + "&message=delete";
    }
}
