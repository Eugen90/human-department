package org.bip.department.controller;

import org.bip.department.domain.Message;
import org.bip.department.domain.Vocation;
import org.bip.department.service.VocationService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class VocationController {
    private final VocationService vocationService;

    public VocationController(VocationService vocationService) {
        this.vocationService = vocationService;
    }

    @GetMapping("/vocation")
    public String getVocations(@RequestParam(name = "employeeId") long employeeId,
                               @RequestParam(name = "message", required = false, defaultValue = "") String message,
                               Model model){
        if (message.equals("save")) model.addAttribute("message", Message.GetSaveMessage(true));
        if (message.equals("delete")) model.addAttribute("message", Message.GetDeleteMessage(true));
        model.addAttribute("vocations", vocationService.findAll(employeeId));
        model.addAttribute("employeeId", employeeId);
        return "vocation";
    }

    @GetMapping("/vocationEdit")
    public String getVocation(@RequestParam(name = "vocationId", required = false, defaultValue = "0") long vocationId,
                               @RequestParam(name = "employeeId") long employeeId,
                               Model model){
        model.addAttribute("vocation", vocationService.findVocation(vocationId));
        model.addAttribute("employeeId", employeeId);
        return "vocationEdit";
    }

    @PostMapping("/vocationEdit")
    public String saveVocation(@RequestParam(name = "employeeId") long employeeId,
                               @Valid Vocation vocation,
                               BindingResult bindingResult,
                               Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("vocation", vocation);
            return "vocationEdit";
        }
        vocationService.saveVocation(vocation, employeeId);

        return "redirect:/vocation?employeeId=" + employeeId + "&message=save";
    }

    @DeleteMapping("/vocationEdit")
    public String deleteVocation(@RequestParam(name = "id") long vocationId,
                                  @RequestParam(name = "employeeId") long employeeId){
        vocationService.deleteVocation(vocationId);

        return "redirect:/vocation?employeeId=" + employeeId + "&message=delete";
    }
}
