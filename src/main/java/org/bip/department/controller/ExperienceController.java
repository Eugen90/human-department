package org.bip.department.controller;

import org.bip.department.domain.Experience;
import org.bip.department.domain.Message;
import org.bip.department.service.ExperienceService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class ExperienceController {
    private final ExperienceService experienceService;

    public ExperienceController(ExperienceService experienceService) {
        this.experienceService = experienceService;
    }

    @GetMapping("/experience")
    public String getExperiences(@RequestParam(name = "employeeId") long employeeId,
                                 @RequestParam(name = "message", required = false, defaultValue = "") String message,
                                 Model model){
        if (message.equals("save")) model.addAttribute("message", Message.GetSaveMessage(true));
        if (message.equals("delete")) model.addAttribute("message", Message.GetDeleteMessage(true));
        model.addAttribute("experiences", experienceService.findAll(employeeId));
        model.addAttribute("employeeId", employeeId);
        return "experience";
    }

    @GetMapping("/experienceEdit")
    public String getExperience(@RequestParam(name = "experienceId", required = false, defaultValue = "0") long experienceId,
                               @RequestParam(name = "employeeId") long employeeId,
                               Model model){
        model.addAttribute("experience", experienceService.findExperience(experienceId));
        model.addAttribute("employeeId", employeeId);
        return "experienceEdit";
    }

    @PostMapping("/experienceEdit")
    public String saveExperience(@RequestParam(name = "employeeId") long employeeId,
                                 @Valid Experience experience,
                                 BindingResult bindingResult,
                                 Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("experience", experience);
            return "experienceEdit";
        }
        experienceService.saveExperience(experience, employeeId);

        return "redirect:/experience?employeeId=" + employeeId + "&message=save";
    }

    @DeleteMapping("/experienceEdit")
    public String deleteTelephone(@RequestParam(name = "id") long experienceId,
                                  @RequestParam(name = "employeeId") long employeeId){
        experienceService.deleteExperience(experienceId);

        return "redirect:/experience?employeeId=" + employeeId + "&message=delete";
    }
}
