package org.bip.department.controller;

import org.bip.department.domain.Awards;
import org.bip.department.domain.Message;
import org.bip.department.service.AwardService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class AwardController {
    private final AwardService awardService;

    public AwardController(AwardService awardService) {
        this.awardService = awardService;
    }

    @GetMapping("/award")
    public String getAwards(@RequestParam(name = "employeeId") long employeeId,
            @RequestParam(name = "message", required = false, defaultValue = "") String message,
            Model model){
        if (message.equals("save")) model.addAttribute("message", Message.GetSaveMessage(true));
        if (message.equals("delete")) model.addAttribute("message", Message.GetDeleteMessage(true));
        model.addAttribute("awards", awardService.findAll(employeeId));
        model.addAttribute("employeeId", employeeId);
        return "award";
    }

    @GetMapping("/awardEdit")
    public String getAward(@RequestParam(name = "awardId", required = false, defaultValue = "0") long awardId,
                           @RequestParam(name = "employeeId") long employeeId,
                           Model model){
        model.addAttribute("award", awardService.findAward(awardId));
        model.addAttribute("employeeId", employeeId);
        return "awardEdit";
    }

    @PostMapping("/awardEdit")
    public String saveAward(@RequestParam(name = "employeeId") long employeeId,
                            @Valid Awards awards,
                            BindingResult bindingResult,
                            Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("award", awards);
            return "awardEdit";
        }
            awardService.saveAward(awards, employeeId);

        return "redirect:/award?employeeId=" + employeeId + "&message=save";
    }

    @DeleteMapping("/awardEdit")
    public String deleteAward(@RequestParam(name = "id") long awardId,
                              @RequestParam(name = "employeeId") long employeeId){
        awardService.deleteAward(awardId);

        return "redirect:/award?employeeId=" + employeeId + "&message=delete";
    }
}
