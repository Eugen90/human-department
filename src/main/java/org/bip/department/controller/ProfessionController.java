package org.bip.department.controller;

import org.bip.department.domain.Message;
import org.bip.department.domain.Profession;
import org.bip.department.service.ProfessionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class ProfessionController {
    private final ProfessionService professionService;

    public ProfessionController(ProfessionService professionService) {
        this.professionService = professionService;
    }

    @GetMapping("/profession")
    public String getProfessions(@RequestParam(name = "employeeId") long employeeId,
                                 @RequestParam(name = "message", required = false, defaultValue = "") String message,
                                 Model model){
        if (message.equals("save")) model.addAttribute("message", Message.GetSaveMessage(true));
        if (message.equals("delete")) model.addAttribute("message", Message.GetDeleteMessage(true));
        model.addAttribute("professions", professionService.findAll(employeeId));
        model.addAttribute("employeeId", employeeId);
        return "profession";
    }

    @GetMapping("/professionEdit")
    public String getProfession(@RequestParam(name = "professionId", required = false, defaultValue = "0") long professionId,
                               @RequestParam(name = "employeeId") long employeeId,
                               Model model){
        model.addAttribute("profession", professionService.findProfession(professionId));
        model.addAttribute("employeeId", employeeId);
        return "professionEdit";
    }

    @PostMapping("/professionEdit")
    public String saveProfession(@RequestParam(name = "employeeId") long employeeId,
                                @Valid Profession profession,
                                BindingResult bindingResult,
                                Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("profession", profession);
            return "professionEdit";
        }
        professionService.saveProfession(profession, employeeId);

        return "redirect:/profession?employeeId=" + employeeId + "&message=save";
    }

    @DeleteMapping("/professionEdit")
    public String deleteProfession(@RequestParam(name = "id") long professionId,
                                  @RequestParam(name = "employeeId") long employeeId){
        professionService.deleteProfession(professionId);

        return "redirect:/profession?employeeId=" + employeeId + "&message=delete";
    }
}
