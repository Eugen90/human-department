package org.bip.department.controller;

import org.bip.department.domain.Message;
import org.bip.department.domain.SocialBenefits;
import org.bip.department.service.SocialBenefitsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class SocialBenefitsController {
    private final SocialBenefitsService benefitsService;

    public SocialBenefitsController(SocialBenefitsService benefitsService) {
        this.benefitsService = benefitsService;
    }

    @GetMapping("/benefits")
    public String getBenefitses(@RequestParam(name = "employeeId") long employeeId,
                                @RequestParam(name = "message", required = false, defaultValue = "") String message,
                                Model model){
        if (message.equals("save")) model.addAttribute("message", Message.GetSaveMessage(true));
        if (message.equals("delete")) model.addAttribute("message", Message.GetDeleteMessage(true));
        model.addAttribute("benefitses", benefitsService.findAll(employeeId));
        model.addAttribute("employeeId", employeeId);
        return "benefits";
    }

    @GetMapping("/benefitsEdit")
    public String getBenefits(@RequestParam(name = "benefitsId", required = false, defaultValue = "0") long benefitsId,
                               @RequestParam(name = "employeeId") long employeeId,
                               Model model){
        model.addAttribute("benefits", benefitsService.findSocialBenefits(benefitsId));
        model.addAttribute("employeeId", employeeId);
        return "benefitsEdit";
    }

    @PostMapping("/benefitsEdit")
    public String saveBenefits(@RequestParam(name = "employeeId") long employeeId,
                               @Valid SocialBenefits socialBenefits,
                               BindingResult bindingResult,
                               Model model){
        if (bindingResult.hasErrors()){
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("benefits", socialBenefits);
            return "benefitsEdit";
        }
        benefitsService.saveBenefits(socialBenefits, employeeId);

        return "redirect:/benefits?employeeId=" + employeeId + "&message=save";
    }

    @DeleteMapping("/benefitsEdit")
    public String deleteTelephone(@RequestParam(name = "id") long benefitsId,
                                  @RequestParam(name = "employeeId") long employeeId){
        benefitsService.deleteBenefits(benefitsId);

        return "redirect:/benefits?employeeId=" + employeeId + "&message=delete";
    }
}
