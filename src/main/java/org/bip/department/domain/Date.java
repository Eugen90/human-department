package org.bip.department.domain;

public abstract class Date {
    public String toUserFormat(String date){
        if (date == null) return "";
        String[] uDate = date.split("-");
        return uDate[2] + "." + uDate[1] + "." + uDate[0];
    };
}
