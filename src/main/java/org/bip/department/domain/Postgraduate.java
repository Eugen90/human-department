package org.bip.department.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Postgraduate {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long postgraduateId;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 50)
    @NotNull
    private String name;

    private int OKIN;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 50)
    @NotNull
    private String institutionName;

    @NotBlank(message = "Поле не может быть пустым")
    @Column(columnDefinition = "DATE")
    @NotNull
    private String endDate;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 20)
    @NotNull
    private String specialty;

    private String okso;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "document_id", unique = true, nullable = false)
    private Document document;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", unique = true, nullable = false)
    private Employee employee;
}
