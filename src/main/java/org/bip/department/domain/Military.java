package org.bip.department.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Military {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long militaryId;

    @Length(max = 15)
    private String stockCategory;

    @Length(max = 15)
    private String rank;

    @Length(max = 15)
    private String section;

    @Length(max = 20)
    private String wus;

    @Length(max = 2)
    private String shelfLife;

    @Length(max = 150)
    private String militaryPlace;

    @Length(max = 50)
    private String commandNumber;

    private boolean totalControl;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", unique = true, nullable = false)
    private Employee employee;
}
