package org.bip.department.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Getter@Setter@RequiredArgsConstructor
public class Kin extends Date{
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long kinId;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 50)
    @NotNull
    private String name;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 250)
    @NotNull
    private String fio;

    @NotBlank(message = "Поле не может быть пустым")
    @Column(columnDefinition = "DATE")
    @NotNull
    private String birthday;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "marital_id", nullable = false)
    private Marital marital;

    @Override
    public String toUserFormat(String date) {
        return super.toUserFormat(date);
    }
}
