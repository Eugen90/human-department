package org.bip.department.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Vocation extends Date{
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long vocationId;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 50)
    @NotNull
    private String vocationName;

    @NotBlank(message = "Поле не может быть пустым")
    @Column(columnDefinition = "DATE")
    @NotNull
    private String workPeriodFrom;

    @NotBlank(message = "Поле не может быть пустым")
    @Column(columnDefinition = "DATE")
    @NotNull
    private String workPeriodUntil;

    private int vacationLength;

    @NotBlank(message = "Поле не может быть пустым")
    @Column(columnDefinition = "DATE")
    @NotNull
    private String startDate;

    @NotBlank(message = "Поле не может быть пустым")
    @Column(columnDefinition = "DATE")
    @NotNull
    private String endDate;

    @Length(max = 50)
    private String base;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", nullable = false)
    private Employee employee;

    @Override
    public String toUserFormat(String date) {
        return super.toUserFormat(date);
    }
}
