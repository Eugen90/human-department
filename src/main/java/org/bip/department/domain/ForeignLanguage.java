package org.bip.department.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class ForeignLanguage {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long languageId;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 25)
    @NotNull
    private String language;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 50)
    @NotNull
    private String degree;

    private int OKIN;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", nullable = false)
    private Employee employee;

}
