package org.bip.department.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Employee extends Date{
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long employeeId;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 50)
    @NotNull
    private String name;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 50)
    @NotNull
    private String lName;

    @Length(max = 50)
    private String pName;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 11)
    @NotNull
    private String birthDay;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 300)
    @NotNull
    private String birthPlace;

    private int OKIN;

    @Length(max = 50)
    private String nationality;

    private int OKATO;

    @Length(max = 50)
    @NotNull
    private String educationStatus;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 500)
    @NotNull
    private String factAddress;

    @Length(max = 7)
    private String postIndex;

    @Length(max = 12)
    private String INN;

    @Length(max = 15)
    private String SNILS;

    @Length(max = 3)
    private String gender;

    @Length(max = 1000)
    private String additionalInformation;

    @Transient
    private int minorChildren;

    @OneToOne (mappedBy="employee", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Postgraduate postgraduate;

    @OneToOne (mappedBy="employee", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Marital marital;

    @OneToOne (mappedBy="employee", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Passport passport;

    @OneToOne (mappedBy="employee", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Military military;

    @OneToOne (mappedBy="employee", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Contract contract;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employee")
    private List<Telephone> telephones;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employee")
    private List<ForeignLanguage> foreignForeignLanguages;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employee")
    private List<Education> education;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employee")
    private List<Profession> profession;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employee")
    private List<Experience> experience;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employee")
    private List<Recruitment> recruitment;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employee")
    private List<Attestation> attestation;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employee")
    private List<Training> training;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employee")
    private List<Retraining> retraining;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employee")
    private List<Awards> awards;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employee")
    private List<SocialBenefits> socialBenefits;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employee")
    private List<Vocation> vocations;

    @Override
    public String toUserFormat(String date) {
        return super.toUserFormat(date);
    }
}
