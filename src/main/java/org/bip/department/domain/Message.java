package org.bip.department.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class Message {
    private String message;
    private boolean isSuccessful;

    public static Message GetDeleteMessage(boolean isSuccessful){
        Message message = new Message();
        message.message = isSuccessful ? "Данные успешно удалены!" : "Не удалось удалить данные!";
        message.isSuccessful = isSuccessful;
        return message;
    }

    public static Message GetSaveMessage(boolean isSuccessful){
        Message message = new Message();
        message.message = isSuccessful ? "Данные успешно сохранены!" : "Не удалось сохранить данные!";
        message.isSuccessful = isSuccessful;
        return message;
    }
}
