package org.bip.department.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Document extends Date{
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private long documentId;

    @Length(max = 20)
    private String name;

    @Length(max = 6)
    private String series;

    @Length(max = 25)
    private String number;

    @Column(columnDefinition = "DATE")
    private String date;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "document")
    private Postgraduate postgraduate;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "document")
    private Education education;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "document")
    private Attestation attestation;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "document")
    private Training training;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "document")
    private Retraining retraining;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "document")
    private Awards awards;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "document")
    private SocialBenefits socialBenefits;

    @Override
    public String toUserFormat(String date) {
        return super.toUserFormat(date);
    }
}
