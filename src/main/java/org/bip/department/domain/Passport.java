package org.bip.department.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Passport {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long passportId;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 15)
    @NotNull
    private String number;

    @NotBlank(message = "Поле не может быть пустым")
    @Column(columnDefinition = "DATE")
    @NotNull
    private String date;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 250)
    @NotNull
    private String issuedBy;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 500)
    @NotNull
    private String passAddress;

    @Length(max = 7)
    private String postIndex;

    @NotBlank(message = "Поле не может быть пустым")
    @Column(columnDefinition = "DATE")
    @NotNull
    private String registrationDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", unique = true, nullable = false)
    private Employee employee;
}
