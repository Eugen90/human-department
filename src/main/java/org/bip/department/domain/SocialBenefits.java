package org.bip.department.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class SocialBenefits{
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long benefitsId;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 25)
    @NotNull
    private String benefitsName;

    @Length(max = 50)
    private String base;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "document_id", unique = true, nullable = false)
    private Document document;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", nullable = false)
    private Employee employee;
}
