package org.bip.department.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Telephone {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long telephoneId;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 18)
    @NotNull
    private String number;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", nullable = false)
    private Employee employee;
}
