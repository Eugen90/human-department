package org.bip.department.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Contract {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long contractId;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 6)
    @NotNull
    private String number;

    @NotBlank(message = "Поле не может быть пустым")
    @Column(columnDefinition = "DATE")
    @NotNull
    private String date;

    @Length(max = 20)
    private String workType;

    @NotBlank(message = "Поле не может быть пустым")
    @Length(max = 20)
    @NotNull
    private String workNature;

    @Column(columnDefinition = "DATE")
    private String dismissalDate;

    @Length(max = 200)
    private String dismissalBase;

    @Length(max = 6)
    private String orderNumber;

    @Column(columnDefinition = "DATE")
    private String orderDate;

    @OneToOne
    @JoinColumn(name = "employee_id", unique = true, nullable = false)
    private Employee employee;
}
