package org.bip.department.repos;

import org.bip.department.domain.Education;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface EducationRepo extends CrudRepository<Education, Long> {
    Iterable<Education> findByEmployeeEmployeeId(long educationId);
    Education findByEducationId(long educationId);

    @Transactional
    @Modifying
    @Query("delete from Education e where e.educationId=:id")
    int deleteByEducationId(long id);
}
