package org.bip.department.repos;

import org.bip.department.domain.Document;
import org.springframework.data.repository.CrudRepository;

public interface DocumentRepo extends CrudRepository<Document, Long> {

}
