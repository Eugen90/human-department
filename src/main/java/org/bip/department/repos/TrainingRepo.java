package org.bip.department.repos;

import org.bip.department.domain.Training;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface TrainingRepo extends CrudRepository<Training, Long> {
    Iterable<Training> findByEmployeeEmployeeId(long employeeId);
    Training findByTrainingId(long trainingId);

    @Transactional
    @Modifying
    @Query("delete from Training t where t.trainingId=:id")
    int deleteByTrainingId(long id);
}
