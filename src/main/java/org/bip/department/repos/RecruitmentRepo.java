package org.bip.department.repos;

import org.bip.department.domain.Recruitment;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface RecruitmentRepo extends CrudRepository<Recruitment, Long> {
    Iterable<Recruitment> findByEmployeeEmployeeId(long employeeId);
    Recruitment findByRecruitmentId(long recruitmentId);

    @Transactional
    @Modifying
    @Query("delete from Recruitment r where r.recruitmentId=:id")
    int deleteByRecruitmentId(long id);
}
