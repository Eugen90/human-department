package org.bip.department.repos;

import org.bip.department.domain.Employee;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface EmployeesRepo extends CrudRepository<Employee, Long> {

    Employee findByEmployeeId(Long id);
    int deleteByEmployeeId(Long id);
}
