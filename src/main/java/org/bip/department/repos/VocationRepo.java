package org.bip.department.repos;

import org.bip.department.domain.Vocation;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface VocationRepo extends CrudRepository<Vocation, Long> {
    Iterable<Vocation> findByEmployeeEmployeeId(long employeeId);
    Vocation findByVocationId(long vocationId);

    @Transactional
    @Modifying
    @Query("delete from Vocation v where v.vocationId=:id")
    int deleteByVocationId(long id);
}
