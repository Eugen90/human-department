package org.bip.department.repos;

import org.bip.department.domain.Awards;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface AwardRepo extends CrudRepository<Awards, Long> {
    Iterable<Awards> findByEmployeeEmployeeId(long employeeId);
    Awards findByAwardId(long awardId);

    @Transactional
    @Modifying
    @Query("delete from Awards a where a.awardId=:id")
    int deleteByAwardId(long id);
}
