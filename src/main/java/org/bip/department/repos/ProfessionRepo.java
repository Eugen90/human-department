package org.bip.department.repos;

import org.bip.department.domain.Profession;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface ProfessionRepo extends CrudRepository<Profession, Long> {
    Iterable<Profession> findByEmployeeEmployeeId(long employeeId);
    Profession findByProfessionId(long professionId);

    @Transactional
    @Modifying
    @Query("delete from Profession p where p.professionId=:id")
    int deleteByProfessionId(long id);
}
