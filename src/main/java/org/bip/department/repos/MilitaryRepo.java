package org.bip.department.repos;

import org.bip.department.domain.Military;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface MilitaryRepo extends CrudRepository<Military, Long> {
    Military findByEmployeeEmployeeId(Long id);
}
