package org.bip.department.repos;

import org.bip.department.domain.Postgraduate;
import org.springframework.data.repository.CrudRepository;

public interface PostgraduateRepo extends CrudRepository<Postgraduate, Long> {
    Postgraduate findByEmployeeEmployeeId(long employeeId);
}
