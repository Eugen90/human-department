package org.bip.department.repos;

import org.bip.department.domain.Marital;
import org.springframework.data.repository.CrudRepository;

public interface MaritalRepo extends CrudRepository<Marital, Long> {
    Marital findByEmployeeEmployeeId(long id);
    Marital findByMaritalId(long id);
}
