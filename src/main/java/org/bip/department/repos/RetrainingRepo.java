package org.bip.department.repos;

import org.bip.department.domain.Retraining;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface RetrainingRepo extends CrudRepository<Retraining, Long> {
    Iterable<Retraining> findByEmployeeEmployeeId(long employeeId);
    Retraining findByRetrainingId(long retrainingId);

    @Transactional
    @Modifying
    @Query("delete from Retraining r where r.retrainingId=:id")
    int deleteByRetrainingId(long id);
}
