package org.bip.department.repos;

import org.bip.department.domain.Passport;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface PassportRepo extends CrudRepository<Passport, Long> {
    Passport findByEmployeeEmployeeId(Long id);
}
