package org.bip.department.repos;

import org.bip.department.domain.SocialBenefits;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface SocialBenefitsRepo extends CrudRepository<SocialBenefits, Long> {
    Iterable<SocialBenefits> findByEmployeeEmployeeId(long employeeId);
    SocialBenefits findByBenefitsId(long benefitsId);

    @Transactional
    @Modifying
    @Query("delete from SocialBenefits sb where sb.benefitsId=:id")
    int deleteByBenefitsId(long id);
}
