package org.bip.department.repos;

import org.bip.department.domain.ForeignLanguage;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface LanguageRepo extends CrudRepository<ForeignLanguage, Long> {
    Iterable<ForeignLanguage> findByEmployeeEmployeeId(long employeeId);
    ForeignLanguage findByLanguageId(long languageId);
    int deleteByLanguageId(long languageId);
}
