package org.bip.department.repos;

import org.bip.department.domain.Telephone;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface TelephoneRepo extends CrudRepository<Telephone, Long> {
    Iterable<Telephone> findByEmployeeEmployeeId(long employeeId);
    Telephone findByTelephoneId(long telephoneId);

    @Transactional
    @Modifying
    @Query("delete from Telephone t where t.telephoneId=:id")
    int deleteByTelephoneId(long id);
}
