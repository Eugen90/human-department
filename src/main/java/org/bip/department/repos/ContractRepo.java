package org.bip.department.repos;

import org.bip.department.domain.Contract;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface ContractRepo extends CrudRepository<Contract, Long> {
    Contract findByEmployeeEmployeeId(long employeeId);
}
