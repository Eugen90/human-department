package org.bip.department.repos;

import org.bip.department.domain.Experience;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface ExperienceRepo extends CrudRepository<Experience, Long> {
    Iterable<Experience> findByEmployeeEmployeeId(long employeeId);
    Experience findByExperienceId(long experienceId);

    @Transactional
    @Modifying
    @Query("delete from Experience e where e.experienceId=:id")
    int deleteByExperienceId(long id);
}
