package org.bip.department.repos;

import org.bip.department.domain.Attestation;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface AttestationRepo extends CrudRepository<Attestation, Long> {
    Iterable<Attestation> findByEmployeeEmployeeId(long employeeId);
    Attestation findByAttestationId(long attestationId);

    @Transactional
    @Modifying
    @Query("delete from Attestation a where a.attestationId=:id")
    int deleteByAttestationId(long id);
}
