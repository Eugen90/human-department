package org.bip.department.repos;

import org.bip.department.domain.Kin;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface KinRepo extends CrudRepository<Kin, Long> {
    Kin findByKinId(long id);

    @Transactional
    @Modifying
    @Query("delete from Kin k where k.kinId=:id")
    int deleteByKinId(long id);
}
