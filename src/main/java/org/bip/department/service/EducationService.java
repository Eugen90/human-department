package org.bip.department.service;

import org.bip.department.domain.Education;
import org.bip.department.domain.Employee;
import org.bip.department.repos.DocumentRepo;
import org.bip.department.repos.EducationRepo;
import org.springframework.stereotype.Service;

@Service
public class EducationService {
    private final EducationRepo educationRepo;
    private final DocumentRepo documentRepo;

    public EducationService(EducationRepo educationRepo, DocumentRepo documentRepo) {
        this.educationRepo = educationRepo;
        this.documentRepo = documentRepo;
    }

    public Iterable<Education> findAll(long employeeId){
        return educationRepo.findByEmployeeEmployeeId(employeeId);
    }

    public Education findEducation(long educationId){
        return educationRepo.findByEducationId(educationId);
    }

    public void saveEducation(Education education, long employeeId){
        if (education.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(employeeId);
            education.setEmployee(employee);
        }
        education.setDocument(documentRepo.save(education.getDocument()));
        educationRepo.save(education);
    }

    public boolean deleteEducation(long id){
        return educationRepo.deleteByEducationId(id) > 0;
    }
}
