package org.bip.department.service;

import org.bip.department.domain.Employee;
import org.bip.department.domain.Experience;
import org.bip.department.repos.ExperienceRepo;
import org.springframework.stereotype.Service;

@Service
public class ExperienceService {
    private final ExperienceRepo experienceRepo;

    public ExperienceService(ExperienceRepo experienceRepo) {
        this.experienceRepo = experienceRepo;
    }

    public Iterable<Experience> findAll(long employeeId){
        return experienceRepo.findByEmployeeEmployeeId(employeeId);
    }

    public Experience findExperience(long experienceId){
        return experienceRepo.findByExperienceId(experienceId);
    }

    public void saveExperience(Experience experience, long employeeId){
        if (experience.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(employeeId);
            experience.setEmployee(employee);
        }
        experienceRepo.save(experience);
    }

    public boolean deleteExperience(long id){
        return experienceRepo.deleteByExperienceId(id) > 0;
    }
}
