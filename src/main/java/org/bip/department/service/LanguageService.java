package org.bip.department.service;

import org.bip.department.domain.Employee;
import org.bip.department.domain.ForeignLanguage;
import org.bip.department.repos.LanguageRepo;
import org.springframework.stereotype.Service;

@Service
public class LanguageService {
    private LanguageRepo languageRepo;

    public LanguageService(LanguageRepo languageRepo) {
        this.languageRepo = languageRepo;
    }

    public Iterable<ForeignLanguage> findAll(long employeeId){
        return languageRepo.findByEmployeeEmployeeId(employeeId);
    }

    public ForeignLanguage findLanguage(long languageId){
        return languageRepo.findByLanguageId(languageId);
    }

    public void saveLanguage(ForeignLanguage foreignLanguage, long employeeId){
        if (foreignLanguage.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(employeeId);
            foreignLanguage.setEmployee(employee);
        }
        languageRepo.save(foreignLanguage);
    }

    public boolean deleteLanguage(long id){
        return languageRepo.deleteByLanguageId(id) > 0;
    }
}
