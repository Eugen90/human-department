package org.bip.department.service;

import org.bip.department.domain.Employee;
import org.bip.department.domain.Vocation;
import org.bip.department.repos.VocationRepo;
import org.springframework.stereotype.Service;

@Service
public class VocationService {
    private final VocationRepo vocationRepo;

    public VocationService(VocationRepo vocationRepo) {
        this.vocationRepo = vocationRepo;
    }

    public Iterable<Vocation> findAll(long employeeId){
        return vocationRepo.findByEmployeeEmployeeId(employeeId);
    }

    public Vocation findVocation(long vocationId){
        return vocationRepo.findByVocationId(vocationId);
    }

    public void saveVocation(Vocation vocation, long employeeId){
        if (vocation.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(employeeId);
            vocation.setEmployee(employee);
        }
        vocationRepo.save(vocation);
    }

    public boolean deleteVocation(long id){
        return vocationRepo.deleteByVocationId(id) > 0;
    }
}
