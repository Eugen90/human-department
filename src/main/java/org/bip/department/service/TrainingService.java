package org.bip.department.service;

import org.bip.department.domain.Employee;
import org.bip.department.domain.Training;
import org.bip.department.repos.DocumentRepo;
import org.bip.department.repos.TrainingRepo;
import org.springframework.stereotype.Service;

@Service
public class TrainingService {
    private final TrainingRepo trainingRepo;
    private final DocumentRepo documentRepo;

    public TrainingService(TrainingRepo trainingRepo, DocumentRepo documentRepo) {
        this.trainingRepo = trainingRepo;
        this.documentRepo = documentRepo;
    }

    public Iterable<Training> findAll(long employeeId){
        return trainingRepo.findByEmployeeEmployeeId(employeeId);
    }

    public Training findTraining(long trainingId){
        return trainingRepo.findByTrainingId(trainingId);
    }

    public void saveTraining(Training training, long employeeId){
        if (training.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(employeeId);
            training.setEmployee(employee);
        }
        training.setDocument(documentRepo.save(training.getDocument()));
        trainingRepo.save(training);
    }

    public boolean deleteTraining(long id){
        return trainingRepo.deleteByTrainingId(id) > 0;
    }
}
