package org.bip.department.service;

import org.bip.department.domain.Employee;
import org.bip.department.domain.Recruitment;
import org.bip.department.repos.RecruitmentRepo;
import org.springframework.stereotype.Service;

@Service
public class RecruitmentService {
    private final RecruitmentRepo recruitmentRepo;

    public RecruitmentService(RecruitmentRepo recruitmentRepo) {
        this.recruitmentRepo = recruitmentRepo;
    }

    public Iterable<Recruitment> findAll(long employeeId){
        return recruitmentRepo.findByEmployeeEmployeeId(employeeId);
    }

    public Recruitment findRecruitment(long recruitmentId){
        return recruitmentRepo.findByRecruitmentId(recruitmentId);
    }

    public void saveRecruitment(Recruitment recruitment, long employeeId){
        if (recruitment.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(employeeId);
            recruitment.setEmployee(employee);
        }
        recruitmentRepo.save(recruitment);
    }

    public boolean deleteRecruitment(long id){
        return recruitmentRepo.deleteByRecruitmentId(id) > 0;
    }
}
