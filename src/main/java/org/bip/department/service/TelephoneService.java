package org.bip.department.service;

import org.bip.department.domain.Employee;
import org.bip.department.domain.Telephone;
import org.bip.department.repos.TelephoneRepo;
import org.springframework.stereotype.Service;

@Service
public class TelephoneService {
    private final TelephoneRepo telephoneRepo;

    public TelephoneService(TelephoneRepo telephoneRepo) {
        this.telephoneRepo = telephoneRepo;
    }

    public Iterable<Telephone> findAll(long employeeId){
        return telephoneRepo.findByEmployeeEmployeeId(employeeId);
    }

    public Telephone findTelephone(long telephoneId){
        return telephoneRepo.findByTelephoneId(telephoneId);
    }

    public void saveTelephone(Telephone telephone, long employeeId){
        if (telephone.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(employeeId);
            telephone.setEmployee(employee);
        }
        telephoneRepo.save(telephone);
    }

    public boolean deleteTelephone(long id){
        return telephoneRepo.deleteByTelephoneId(id) > 0;
    }
}
