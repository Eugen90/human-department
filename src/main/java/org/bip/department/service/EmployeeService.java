package org.bip.department.service;

import org.bip.department.domain.Employee;
import org.bip.department.repos.EmployeesRepo;
import org.springframework.stereotype.Service;


@Service
public class EmployeeService {
    private EmployeesRepo employeesRepo;

    public EmployeeService(EmployeesRepo employeesRepo) {
        this.employeesRepo = employeesRepo;
    }

    public Iterable<Employee> findAll() {
        return employeesRepo.findAll();
    }

    public Employee findEmployee(long id) {
        return employeesRepo.findByEmployeeId(id);
    }

    public Employee saveEmployee(Employee employee) {
        return employeesRepo.save(employee);
    }

    public boolean deleteEmployee(String id) {
        return employeesRepo.deleteByEmployeeId(Long.parseLong(id)) > 0;
    }
}
