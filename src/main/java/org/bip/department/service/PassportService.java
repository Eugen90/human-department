package org.bip.department.service;

import org.bip.department.domain.Employee;
import org.bip.department.domain.Passport;
import org.bip.department.repos.PassportRepo;
import org.springframework.stereotype.Service;

@Service
public class PassportService {
    private PassportRepo passportRepo;

    public PassportService(PassportRepo passportRepo) {
        this.passportRepo = passportRepo;
    }

    public Passport getEmployeePassport(Long employeeId){
        return passportRepo.findByEmployeeEmployeeId(employeeId);
    }

    public void savePassport(Passport passport, long employeeId){
        if (passport.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(employeeId);
            passport.setEmployee(employee);
        }
        passportRepo.save(passport);
    }
}
