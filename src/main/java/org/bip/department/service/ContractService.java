package org.bip.department.service;

import org.bip.department.domain.Contract;
import org.bip.department.domain.Employee;
import org.bip.department.repos.ContractRepo;
import org.springframework.stereotype.Service;

@Service
public class ContractService {
    private ContractRepo contractRepo;

    public ContractService(ContractRepo contractRepo) {
        this.contractRepo = contractRepo;
    }

    public Contract findContract(long employeeId){
        return contractRepo.findByEmployeeEmployeeId(employeeId);
    }

    public void saveContract(Contract contract, long employeeId) {
        if (contract.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(employeeId);
            contract.setEmployee(employee);
        }
        contractRepo.save(contract);
    }
}
