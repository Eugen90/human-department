package org.bip.department.service;

import org.bip.department.domain.Employee;
import org.bip.department.domain.Postgraduate;
import org.bip.department.repos.DocumentRepo;
import org.bip.department.repos.PostgraduateRepo;
import org.springframework.stereotype.Service;

@Service
public class PostgraduateService {
    private final PostgraduateRepo postgraduateRepo;
    private final DocumentRepo documentRepo;

    public PostgraduateService(PostgraduateRepo postgraduateRepo, DocumentRepo documentRepo) {
        this.postgraduateRepo = postgraduateRepo;
        this.documentRepo = documentRepo;
    }

    public Postgraduate findPostgraduate(long employeeId){
        return postgraduateRepo.findByEmployeeEmployeeId(employeeId);
    }

    public void savePostgraduate(Postgraduate postgraduate, long employeeId){
        if (postgraduate.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(employeeId);
            postgraduate.setEmployee(employee);
        }
        postgraduate.setDocument(documentRepo.save(postgraduate.getDocument()));
        postgraduateRepo.save(postgraduate);
    }
}
