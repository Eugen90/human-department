package org.bip.department.service;

import org.bip.department.domain.Employee;
import org.bip.department.domain.SocialBenefits;
import org.bip.department.repos.DocumentRepo;
import org.bip.department.repos.SocialBenefitsRepo;
import org.springframework.stereotype.Service;

@Service
public class SocialBenefitsService {
    private final SocialBenefitsRepo benefitsRepo;
    private final DocumentRepo documentRepo;

    public SocialBenefitsService(SocialBenefitsRepo benefitsRepo, DocumentRepo documentRepo) {
        this.benefitsRepo = benefitsRepo;
        this.documentRepo = documentRepo;
    }

    public Iterable<SocialBenefits> findAll(long employeeId){
        return benefitsRepo.findByEmployeeEmployeeId(employeeId);
    }

    public SocialBenefits findSocialBenefits(long benefitsId){
        return benefitsRepo.findByBenefitsId(benefitsId);
    }

    public void saveBenefits(SocialBenefits benefits, long employeeId){
        if (benefits.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(employeeId);
            benefits.setEmployee(employee);
        }
        benefits.setDocument(documentRepo.save(benefits.getDocument()));
        benefitsRepo.save(benefits);
    }

    public boolean deleteBenefits(long id){
        return benefitsRepo.deleteByBenefitsId(id) > 0;
    }
}
