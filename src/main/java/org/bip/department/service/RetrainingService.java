package org.bip.department.service;

import org.bip.department.domain.Employee;
import org.bip.department.domain.Retraining;
import org.bip.department.repos.DocumentRepo;
import org.bip.department.repos.RetrainingRepo;
import org.springframework.stereotype.Service;

@Service
public class RetrainingService {
    private final RetrainingRepo retrainingRepo;
    private final DocumentRepo documentRepo;

    public RetrainingService(RetrainingRepo retrainingRepo, DocumentRepo documentRepo) {
        this.retrainingRepo = retrainingRepo;
        this.documentRepo = documentRepo;
    }

    public Iterable<Retraining> findAll(long employeeId){
        return retrainingRepo.findByEmployeeEmployeeId(employeeId);
    }

    public Retraining findRetraining(long retrainingId){
        return retrainingRepo.findByRetrainingId(retrainingId);
    }

    public void saveRetraining(Retraining retraining, long employeeId){
        if (retraining.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(employeeId);
            retraining.setEmployee(employee);
        }
        retraining.setDocument(documentRepo.save(retraining.getDocument()));
        retrainingRepo.save(retraining);
    }

    public boolean deleteRetraining(long id){
        return retrainingRepo.deleteByRetrainingId(id) > 0;
    }
}
