package org.bip.department.service;

import org.bip.department.domain.Attestation;
import org.bip.department.domain.Employee;
import org.bip.department.repos.AttestationRepo;
import org.bip.department.repos.DocumentRepo;
import org.springframework.stereotype.Service;

@Service
public class AttestationService {
    private final AttestationRepo attestationRepo;
    private final DocumentRepo documentRepo;

    public AttestationService(AttestationRepo attestationRepo, DocumentRepo documentRepo) {
        this.attestationRepo = attestationRepo;
        this.documentRepo = documentRepo;
    }

    public Iterable<Attestation> findAll(long employeeId){
        return attestationRepo.findByEmployeeEmployeeId(employeeId);
    }

    public Attestation findAttestation(long attestationId){
        return attestationRepo.findByAttestationId(attestationId);
    }

    public void saveAttestation(Attestation attestation, long employeeId){
        if (attestation.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(employeeId);
            attestation.setEmployee(employee);
        }
        attestation.setDocument(documentRepo.save(attestation.getDocument()));
        attestationRepo.save(attestation);
    }

    public boolean deleteAttestation(long id){
        return attestationRepo.deleteByAttestationId(id) > 0;
    }
}
