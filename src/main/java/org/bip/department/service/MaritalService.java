package org.bip.department.service;

import org.bip.department.domain.Employee;
import org.bip.department.domain.Kin;
import org.bip.department.domain.Marital;
import org.bip.department.repos.KinRepo;
import org.bip.department.repos.MaritalRepo;
import org.springframework.stereotype.Service;

@Service
public class MaritalService {
    private MaritalRepo maritalRepo;
    private KinRepo kinRepo;

    public MaritalService(MaritalRepo maritalRepo, KinRepo kinRepo) {
        this.maritalRepo = maritalRepo;
        this.kinRepo = kinRepo;
    }

    public Marital getEmployeeMarital(long employeeId){
        return maritalRepo.findByEmployeeEmployeeId(employeeId);
    }

    public void saveMarital(Marital marital, long employeeId){
        if (marital.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(employeeId);
            marital.setEmployee(employee);
        }
        maritalRepo.save(marital);
    }

    public Kin getKin(long kinId){
        return kinRepo.findByKinId(kinId);
    }

    public Kin saveKin(Kin kin, long maritalId){
        if (kin.getMarital() == null){
            Marital marital = new Marital();
            marital.setMaritalId(maritalId);
            kin.setMarital(marital);
        }
        return kinRepo.save(kin);
    }

    public boolean deleteKin(long kinId){
        return  kinRepo.deleteByKinId(kinId) > 0;
    }
}
