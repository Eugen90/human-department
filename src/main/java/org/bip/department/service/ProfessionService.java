package org.bip.department.service;

import org.bip.department.domain.Employee;
import org.bip.department.domain.Profession;
import org.bip.department.repos.ProfessionRepo;
import org.springframework.stereotype.Service;

@Service
public class ProfessionService {
    private final ProfessionRepo professionRepo;

    public ProfessionService(ProfessionRepo professionRepo) {
        this.professionRepo = professionRepo;
    }

    public Iterable<Profession> findAll(long employeeId){
        return professionRepo.findByEmployeeEmployeeId(employeeId);
    }

    public Profession findProfession(long professionId){
        return professionRepo.findByProfessionId(professionId);
    }

    public void saveProfession(Profession profession, long employeeId){
        if (profession.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(employeeId);
            profession.setEmployee(employee);
        }
        professionRepo.save(profession);
    }

    public boolean deleteProfession(long id){
        return professionRepo.deleteByProfessionId(id) > 0;
    }
}
