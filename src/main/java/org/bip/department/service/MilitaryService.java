package org.bip.department.service;

import org.bip.department.domain.Employee;
import org.bip.department.domain.Military;
import org.bip.department.repos.MilitaryRepo;
import org.springframework.stereotype.Service;

@Service
public class MilitaryService {
    private MilitaryRepo militaryRepo;

    public MilitaryService(MilitaryRepo militaryRepo) {
        this.militaryRepo = militaryRepo;
    }

    public Military getEmployeeMilitary(long employeeId){
        return militaryRepo.findByEmployeeEmployeeId(employeeId);
    }

    public void saveMilitary(Military military, long id){
        if (military.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(id);
            military.setEmployee(employee);
        }
        militaryRepo.save(military);
    }
}
