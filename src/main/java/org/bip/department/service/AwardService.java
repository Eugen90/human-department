package org.bip.department.service;

import org.bip.department.domain.Awards;
import org.bip.department.domain.Employee;
import org.bip.department.repos.AwardRepo;
import org.bip.department.repos.DocumentRepo;
import org.springframework.stereotype.Service;

@Service
public class AwardService {
    private final AwardRepo awardRepo;
    private final DocumentRepo documentRepo;

    public AwardService(AwardRepo awardRepo, DocumentRepo documentRepo) {
        this.awardRepo = awardRepo;
        this.documentRepo = documentRepo;
    }

    public Iterable<Awards> findAll(long employeeId){
        return awardRepo.findByEmployeeEmployeeId(employeeId);
    }

    public Awards findAward(long awardId){
        return awardRepo.findByAwardId(awardId);
    }

    public void saveAward(Awards award,long employeeId){
        if (award.getEmployee() == null){
            Employee employee = new Employee();
            employee.setEmployeeId(employeeId);
            award.setEmployee(employee);
        }
        award.setDocument(documentRepo.save(award.getDocument()));
        awardRepo.save(award);
    }

    public boolean deleteAward(long id){
        return awardRepo.deleteByAwardId(id) > 0;
    }
}
