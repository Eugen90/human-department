<#import "parts/common.ftl" as c>

<@c.page>
    <h3 align="center">
        <#if employee??>Редактирование данных сотрудника ${employee.getLName()}
            <#else>Создание нового сотрудника
        </#if>
    </h3>

    <form method = "post">
        <input type="hidden" name="employeeId" value="<#if employee??>${employee.getEmployeeId()} <#else> 0</#if>">
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="lName">Фамилия</label>
                <input type="text" class="form-control ${(lNameError??)?string('is-invalid', '')}" name="lName" maxlength="50"
                       id="lName" placeholder="Фамилия" value="<#if employee??>${employee.getLName()}</#if>">
                <#if lNameError??>
                    <div class="invalid-feedback">
                        ${lNameError}
                    </div>
                </#if>
            </div>
            <div class="form-group col-md-4">
                <label for="name">Имя</label>
                <input type="text" class="form-control ${(nameError??)?string('is-invalid', '')}" name="name" id="name"
                       maxlength="50" placeholder="Имя" value="<#if employee??>${employee.getName()}</#if>">
                <#if nameError??>
                    <div class="invalid-feedback">
                        ${nameError}
                    </div>
                </#if>
            </div>
            <div class="form-group  col-md-4">
                <label for="pName">Отчество</label>
                <input type="text" class="form-control" name="pName" id="pName" placeholder="Отчество" maxlength="50"
                       value="<#if employee??>${employee.getPName()}</#if>">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="birthDay">Дата рождения</label>
                <input type="date" class="form-control ${(birthDayError??)?string('is-invalid', '')}"
                       name="birthDay" id="birthDay" value="<#if employee??>${employee.getBirthDay()}</#if>">
                <#if birthDayError??>
                    <div class="invalid-feedback">
                        ${birthDayError}
                    </div>
                </#if>
            </div>
            <div class="form-group col-md-8">
                <label for="birthPlace">Место рождения</label>
                <input type="text" class="form-control ${(birthPlaceError??)?string('is-invalid', '')}" name="birthPlace" maxlength="300"
                       id="birthPlace" placeholder="Место рождения" value="<#if employee??>${employee.getBirthPlace()}</#if>">
                <#if birthPlaceError??>
                    <div class="invalid-feedback">
                        ${birthPlaceError}
                    </div>
                </#if>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="nationality">Гражданство</label>
                <input type="text" class="form-control" name="nationality" id="nationality" placeholder="Гражданство"
                       maxlength="50" value="<#if employee??>${employee.getNationality()}</#if>">
            </div>
            <div class="form-group col-md-4">
                <label for="okato">ОКАТО</label>
                <input type="number" class="form-control" name="OKATO" id="okato" placeholder="ОКАТО"
                       value="<#if employee??>${employee.getOKATO()}</#if>">
            </div>
            <div class="form-group  col-md-4">
                <label for="okin">ОКИН</label>
                <input type="number" class="form-control" name="OKIN" id="okin" placeholder="ОКИН"
                       value="<#if employee??>${employee.getOKIN()}</#if>">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-8">
                <label for="address">Фактический адрес</label>
                <input type="text" class="form-control ${(factAddressError??)?string('is-invalid', '')}"  maxlength="500"
                       name="factAddress" id="address" placeholder="Фактический адрес"
                       value="<#if employee??>${employee.getFactAddress()}</#if>">
                <#if factAddressError??>
                    <div class="invalid-feedback">
                        ${factAddressError}
                    </div>
                </#if>
            </div>
            <div class="form-group col-md-4">
                <label for="index">Почтовый индекс</label>
                <input type="text" class="form-control mask-index" name="PostIndex" id="index" placeholder="Почтовый индекс"
                       value="<#if employee??>${employee.getPostIndex()}</#if>">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="education">Образование</label>
                <select class="form-control" name="educationStatus" id="education">
                    <option <#if employee??><#if employee.getEducationStatus()=="Среднее общее"> selected </#if></#if>>
                        Среднее общее</option>
                    <option <#if employee??><#if employee.getEducationStatus()=="Среднее полное"> selected </#if></#if>>
                        Среднее полное</option>
                    <option <#if employee??><#if employee.getEducationStatus()=="Начальное профессиональное"> selected </#if></#if>>
                        Начальное профессиональное</option>
                    <option <#if employee??><#if employee.getEducationStatus()=="Среднее профессиональное"> selected </#if></#if>>
                        Среднее профессиональное</option>
                    <option <#if employee??><#if employee.getEducationStatus()=="Высшее профессиональное"> selected </#if></#if>>
                        Высшее профессиональное</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="inn">ИНН</label>
                <input type="text" class="form-control mask-inn" name="INN" id="inn" placeholder="ИНН"
                       value="<#if employee??>${employee.getINN()}</#if>"/>
            </div>
            <div class="form-group col-md-4">
                <label for="snils">СНИЛС</label>
                <input type="text" class="form-control mask-snils" name="SNILS" id="snils" placeholder="СНИЛС"
                       value="<#if employee??>${employee.getSNILS()}</#if>"/>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="gender">Пол</label>
                <select class="form-control" name="gender" id="gender">
                    <option <#if employee??><#if employee.getGender()=="Муж"> selected </#if></#if>>Муж</option>
                    <option <#if employee??><#if employee.getGender()=="Жен"> selected </#if></#if>>Жен</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="info">Дополнительные сведения</label>
            <input type="text" class="form-control" name="additionalInformation" id="info" placeholder="Дополнительные сведения"
                   maxlength="1000" value="<#if employee??>${employee.getAdditionalInformation()}</#if>"/>
        </div>
        <input type="hidden" name="_csrf" value="${_csrf.token}" />
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Сохранить</button>
            <a class="btn btn-primary" href="/staffList" role="button">К списку сотрудников</a>

        </div>
    </form>
</@c.page>