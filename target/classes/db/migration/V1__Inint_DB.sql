create table hibernate_sequences (
    sequence_name varchar(255) not null,
    next_val int8,
    primary key (sequence_name));


create table attestation (
    attestation_id int8 not null,
    base varchar(50),
    date varchar(11) not null,
    decision varchar(50),
    document_id int8 not null,
    employee_id int8 not null,
    primary key (attestation_id));

create table awards (
    award_id int8 not null,
    award_name varchar(25) not null,
    document_id int8 not null,
    employee_id int8 not null,
    primary key (award_id));

create table contract (
    contract_id int8 not null,
    date varchar(11) not null,
    dismissal_base varchar(200),
    dismissal_date DATE,
    number varchar(6) not null,
    order_date varchar(11),
    order_number varchar(6),
    work_nature varchar(20) not null,
    work_type varchar(20),
    employee_id int8 not null,
    primary key (contract_id));

create table document (
     document_id int8 not null,
     date varchar(11),
     name varchar(20),
     number varchar(25),
     series varchar(6),
     primary key (document_id));

create table education (
    education_id int8 not null,
    end_date varchar(11) not null,
    institution_name varchar(50) not null,
    okso varchar(255),
    qualification varchar(20) not null,
    specialty varchar(20) not null,
    document_id int8 not null,
    employee_id int8 not null,
    primary key (education_id));

create table employee (
    employee_id int8 not null,
    inn varchar(12),
    okato integer not null,
    okin integer not null,
    snils varchar(15),
    additional_information varchar(1000),
    birth_day varchar(11) not null,
    birth_place varchar(300) not null,
    education_status varchar(50) not null,
    fact_address varchar(500) not null,
    gender varchar(3),
    l_name varchar(50) not null,
    name varchar(50) not null,
    nationality varchar(50),
    p_name varchar(50),
    post_index varchar(7),
    primary key (employee_id));

create table experience (
    experience_id int8 not null,
    day integer not null,
    month integer not null,
    name varchar(50) not null,
    year integer not null,
    employee_id int8 not null,
    primary key (experience_id));

create table foreign_language (
    language_id int8 not null,
    okin integer not null,
    degree varchar(50) not null,
    language varchar(25) not null,
    employee_id int8 not null,
    primary key (language_id));

create table kin (
    kin_id bigint not null,
    birthday varchar(11) not null,
    fio varchar(250) not null,
    name varchar(50) not null,
    marital_id int8 not null,
    primary key (kin_id));

create table marital (
    marital_id bigint not null,
    okin integer not null,
    status varchar(50) not null,
    employee_id int8 not null,
    primary key (marital_id));

create table military (
    military_id int8 not null,
    command_number varchar(50),
    military_place varchar(150),
    rank varchar(15),
    section varchar(15),
    shelf_life varchar(2),
    stock_category varchar(15),
    total_control boolean not null,
    wus varchar(20),
    employee_id int8 not null,
    primary key (military_id));

create table passport (
    passport_id int8 not null,
    date varchar(11) not null,
    issued_by varchar(250) not null,
    number varchar(15) not null,
    pass_address varchar(500) not null,
    post_index varchar(7),
    registration_date varchar(11) not null,
    employee_id int8 not null,
    primary key (passport_id));

create table postgraduate (
    postgraduate_id int8 not null,
    okin integer not null,
    end_date varchar(11) not null,
    institution_name varchar(50) not null,
    name varchar(50) not null,
    okso varchar(255),
    specialty varchar(20) not null,
    document_id int8 not null,
    employee_id int8 not null,
    primary key (postgraduate_id));

create table profession (
    profession_id int8 not null,
    okpdtr varchar(10),
    name varchar(50) not null,
    employee_id int8 not null,
    primary key (profession_id));

create table recruitment (
    recruitment_id int8 not null,
    base varchar(50),
    date varchar(11) not null,
    position varchar(50) not null,
    salary varchar(10),
    subdivision varchar(50),
    employee_id int8 not null,
    primary key (recruitment_id));

create table retraining (
    retraining_id int8 not null,
    base varchar(50),
    end_date varchar(11) not null,
    specialty varchar(20) not null,
    start_date varchar(11) not null,
    document_id int8 not null,
    employee_id int8 not null,
    primary key (retraining_id));

create table social_benefits (
    benefits_id int8 not null,
    base varchar(50),
    benefits_name varchar(25) not null,
    document_id int8 not null,
    employee_id int8 not null,
    primary key (benefits_id));

create table telephone (
    telephone_id int8 not null,
    number varchar(18) not null,
    employee_id int8 not null,
    primary key (telephone_id));

create table training (
    training_id int8 not null,
    base varchar(50),
    end_date varchar(11) not null,
    institution_name varchar(50) not null,
    start_date varchar(11) not null,
    training_type varchar(25) not null,
    document_id int8 not null,
    employee_id int8 not null,
    primary key (training_id));

create table usr (
    id int8 not null,
    active boolean not null,
    nick_name varchar(150) not null,
    password varchar(1000) not null,
    role varchar(6) not null,
    username varchar(50) not null,
    primary key (id));

create table vocation (
    vocation_id int8 not null,
    base varchar(50),
    end_date varchar(11) not null,
    start_date varchar(11) not null,
    vacation_length integer not null,
    vocation_name varchar(50) not null,
    work_period_from varchar(11) not null,
    work_period_until varchar(11) not null,
    employee_id int8 not null,
    primary key (vocation_id));

alter table attestation
    add constraint attestation_u unique (document_id);

alter table awards
    add constraint awards_u unique (document_id);

alter table contract
    add constraint contract_u unique (employee_id);

alter table education
    add constraint education_u unique (document_id);

alter table marital
    add constraint marital_u unique (employee_id);

alter table military
    add constraint military_u unique (employee_id);

alter table passport
    add constraint passport_u unique (employee_id);

alter table postgraduate
    add constraint postgraduate_ud unique (document_id);

alter table postgraduate
    add constraint postgraduate_ue unique (employee_id);

alter table retraining
    add constraint retraining_u unique (document_id);

alter table social_benefits
    add constraint benefits_u unique (document_id);

alter table training
    add constraint training_u unique (document_id);

alter table attestation
    add constraint attestation_fkd
    foreign key (document_id)
    references document (document_id)
    on delete cascade on update cascade;


alter table attestation
    add constraint attestation_fke
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;

alter table awards
    add constraint awards_fkd
    foreign key (document_id)
    references document (document_id)
    on delete cascade on update cascade;

alter table awards
    add constraint awards_fke
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;

alter table contract
    add constraint contract_fk
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;

alter table education
    add constraint education_fkd
    foreign key (document_id)
    references document (document_id)
    on delete cascade on update cascade;

alter table education
    add constraint education_fke
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;

alter table experience
    add constraint experience_fk
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;

alter table foreign_language
    add constraint language_fk
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;

alter table kin
    add constraint kin_fk
    foreign key (marital_id)
    references marital (marital_id)
    on delete cascade on update cascade;

alter table marital
    add constraint marital_fk
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;

alter table military
    add constraint military_fk
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;

alter table passport
    add constraint passport_fk
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;

alter table postgraduate
    add constraint postgraduate_fkd
    foreign key (document_id)
    references document (document_id)
    on delete cascade on update cascade;

alter table postgraduate
    add constraint postgraduate_fke
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;

alter table profession
    add constraint profession_fk
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;

alter table recruitment
    add constraint recruitment_fk
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;

alter table retraining
    add constraint retraining_fkd
    foreign key (document_id)
    references document (document_id)
    on delete cascade on update cascade;

alter table retraining
    add constraint retraining_fke
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;

alter table social_benefits
    add constraint benefits_fkd
    foreign key (document_id)
    references document (document_id)
    on delete cascade on update cascade;

alter table social_benefits
    add constraint benefits_fke
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;

alter table telephone
    add constraint telephone_fk
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;

alter table training
    add constraint training_fkd
    foreign key (document_id)
    references document (document_id)
    on delete cascade on update cascade;

alter table training
    add constraint training_fke
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;

alter table vocation
    add constraint vocation_fk
    foreign key (employee_id)
    references employee (employee_id)
    on delete cascade on update cascade;