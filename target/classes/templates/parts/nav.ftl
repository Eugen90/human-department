<nav class="d-none d-xl-block col-xl-2" id="bd-docs-nav" aria-label="Main navigation">
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12 mb-1" href="/passport?employeeId=<#if employee??>${employee.getEmployeeId()}
        </#if>">Паспорт</a>
    </div>
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12 mb-1" href="/marital?employeeId=<#if employee??>${employee.getEmployeeId()}
        </#if>">Семья</a>
    </div>
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12 mb-1" href="/military?employeeId=<#if employee??>${employee.getEmployeeId()}
        </#if>">Воинский учет</a>
    </div>
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12 mb-1" href="/contract?employeeId=<#if employee??>${employee.getEmployeeId()}
        </#if>">Договор</a>
    </div>
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12 mb-1" href="/language?employeeId=<#if employee??>${employee.getEmployeeId()}
        </#if>">Иностранные языки</a>
    </div>
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12 mb-1" href="/telephone?employeeId=<#if employee??>${employee.getEmployeeId()}
        </#if>">Телефоны</a>
    </div>
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12 mb-1" href="/education?employeeId=<#if employee??>${employee.getEmployeeId()}
        </#if>">Образование</a>
    </div>
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12 mb-1" href="/postgraduate?employeeId=<#if employee??>${employee.getEmployeeId()}
</#if>">Послевузовское образование</a>
    </div>
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12 mb-1" href="/profession?employeeId=<#if employee??>${employee.getEmployeeId()}
        </#if>">Профессия</a>
    </div>
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12 mb-1" href="/experience?employeeId=<#if employee??>${employee.getEmployeeId()}
        </#if>">Стаж</a>
    </div>
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12 mb-1" href="/recruitment?employeeId=<#if employee??>${employee.getEmployeeId()}
        </#if>">Работа</a>
    </div>
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12 mb-1" href="/attestation?employeeId=<#if employee??>${employee.getEmployeeId()}
        </#if>">Аттестация</a>
    </div>
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12 mb-1" href="/retraining?employeeId=<#if employee??>${employee.getEmployeeId()}
        </#if>">Переподготовка</a>
    </div>
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12 mb-1" href="/training?employeeId=<#if employee??>${employee.getEmployeeId()}
        </#if>">Повышение квалификации</a>
    </div>
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12 mb-1" href="/award?employeeId=<#if employee??>${employee.getEmployeeId()}
        </#if>">Награды</a>
    </div>
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12 mb-1" href="/benefits?employeeId=<#if employee??>${employee.getEmployeeId()}
        </#if>">Социальные льготы</a>
    </div>
    <div class="bd-toc-item">
        <a class="btn btn-primary col-12" href="/vocation?employeeId=<#if employee??>${employee.getEmployeeId()}
        </#if>">Отпуски</a>
    </div>
       </nav>