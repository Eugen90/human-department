<#assign
    known = Session.SPRING_SECURITY_CONTEXT??
>

<#if known>
    <#assign
        user = Session.SPRING_SECURITY_CONTEXT.authentication.principal
        name = user.getNickName()
        userName = user.getUsername()
        isAdmin = user.isAdmin()
    >
<#else>
    <#assign
        name = ""
        userName = ""
        isAdmin = false
    >
</#if>