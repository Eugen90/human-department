<#macro page>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Отдел кадров</title>
        <link rel="shortcut icon" href="/images/b&p_logo.png" type="image/png">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
              integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    </head>
    <body>
    <#include "navbar.ftl">
    <header>
        <section class="jumbotron text-center">
            <div class="container">
                <img width="100" height="100" src="https://spobip-bel.ru/index/wp-content/uploads/2019/10/33.png">
                <h1 class="jumbotron-heading"> ТЕХНИКУМ "БИЗНЕС И ПРАВО"</h1>
                <p class="lead text-muted">ЧАСТНОЕ УЧРЕЖДЕНИЕ ПРОФЕССИОНАЛЬНАЯ ОБРАЗОВАТЕЛЬНАЯ ОРГАНИЗАЦИЯ ТЕХНИКУМ “БИЗНЕС И ПРАВО”</p>
            </div>
        </section>
    </header>
    <div class="container-fluid">
        <div class="row flex-xl-nowrap">
            <#if employee??><#include "nav.ftl"></#if>
            <main class="<#if employee??>col-md-9 col-xl-8 py-md-3 pl-md-5 <#else>container</#if>" role="main">
                <#if message??>
                <div class="alert <#if message.successful> alert-success <#else> alert-danger</#if>" role="alert">
                    ${message.message}
                    </div></#if>
                <#nested>
            </main>
        </div>
    </div>
    <#include "deleteDialog.ftlh">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="https://snipp.ru/cdn/maskedinput/jquery.maskedinput.min.js"></script>
    <script>
        $('.mask-index').mask('999-999');
        $('.mask-inn').mask('999999999999');
        $('.mask-snils').mask('999-999-999 99');
        $('.mask-passport').mask('9999 999999')
        $('.mask-telephone').mask('+7(999)999-9999')
    </script>
    <script>
        function tableSearch() {
            let phrase = document.getElementById('search-text');
            let table = document.getElementById('table');
            let regPhrase = new RegExp(phrase.value, 'i');
            let flag = false;
            for (let i = 1; i < table.rows.length; i++) {
                flag = false;
                for (let j = table.rows[i].cells.length - 1; j >= 0; j--) {
                    flag = regPhrase.test(table.rows[i].cells[j].innerHTML);
                    if (flag) break;
                }
                if (flag) {
                    table.rows[i].style.display = "";
                } else {
                    table.rows[i].style.display = "none";
                }
            }
        }
    </script>
    <script>
        $('#confirm-delete').on('click', '.btn-ok', function(e) {
            let $modalDiv = $(e.delegateTarget);
            let id = $(this).data('recordId');
            let url = $(this).data('redirectUrl');
            let token = $(this).data("token");
            let employeeId = $(this).data("employeeId")
            let body = new FormData(document.forms.delete);
            body.append("id", id);
            body.append("_csrf", token);
            body.append("employeeId", employeeId);
            let x = new XMLHttpRequest();
            x.open("DELETE", url);
            x.send(body);
            $modalDiv.addClass('loading');
            setTimeout(function() {
                $modalDiv.modal('hide').removeClass('loading');
                location.reload();
            }, 1000)
        });
        $('#confirm-delete').on('show.bs.modal', function(e) {
            var data = $(e.relatedTarget).data();
            $('.btn-ok', this).data('recordId', data.recordId);
            $('.btn-ok', this).data('redirectUrl', data.redirectUrl);
            $('.btn-ok', this).data('token', data.token);
            $('.btn-ok', this).data('employeeId', data.employeeId);
        });
    </script>
    </body>
    </html>;
</#macro>